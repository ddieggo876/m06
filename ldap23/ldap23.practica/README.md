# LDAP:PRACTICA
Pujar-la al git

Pujar-la al dockerhub

Generar els README.md apropiats

Crear un schema amb:

    Un nou objecte STRUCTURAL

    Un nou objecte AUXILIARU

    Cada objecte ha de tenir almenys 3 nous atributs.

    Heu d’utilitzar almenys els atributes de tipus boolean, foto (imatge jpeg) i binary per contenir documents pdf.

Crear una nova ou anomenada practica.

Crear almenys 3 entitats noves dins de ou=practica que siguin dels objectClass definits en l’schema. 

    Assegurar-se de omplir amb dades reals la foto i el pdf.

Eliminar del slapd.conf tots els schema que no facin falta, deixar només els imprescindibles

Visualitzeu amb phpldapadmin les dades, observeu l’schema i verifiqueu la foto i el pdf.

# SCHEMA

Creem el archiu ".schema" on definirem els objectes STRUCTURAL i AUXILIAR sobre productes electronic:
~~~
# Definición del objeto estructural "miProducto"

# Atributo para el nombre de un producto
attributetype ( 1.1.2.1.1.1 NAME 'productName'
    DESC 'Nombre del producto'
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
    SINGLE-VALUE )

# Atributo para el manual del producto en documentos PDF
attributetype ( 1.1.2.1.1.2 NAME 'productInform'
    DESC 'Documento PDF sobre el producto'
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.5
    SINGLE-VALUE)

# Atributo para fotos del producto en formato JPEG
attributetype ( 1.1.2.1.1.3 NAME 'productPhoto'
    DESC 'Foto en formato JPEG deun producto'
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.28
    SINGLE-VALUE )

#Atributo para comprovar si hay stoc del producto
attributetype ( 1.1.2.1.1.4 NAME 'productStock'
    DESC 'Diu si hi ha stock del producte TRUE/FALSE'
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
    SINGLE-VALUE)

# Objeto para representar a los productos
objectclass ( 1.1.2.1.1
    NAME 'producte'
    SUP top
    STRUCTURAL
    MUST ( productName $ productInform)
    MAY (productPhoto $ productStock)
    )
#-----------------------------------------------------------------------------------
#
attributetype ( 1.1.2.2.1.1 NAME 'productRating'
    DESC 'Nota del producte'
    EQUALITY integerMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
    SINGLE-VALUE )

attributetype ( 1.1.2.2.1.2 NAME 'productFrom'
    DESC 'Provinencia del producte'
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
    SINGLE-VALUE )

attributetype ( 1.1.2.2.1.3 NAME 'productBrand'
    DESC 'Marca de un producte'
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
    SINGLE-VALUE )

objectclass ( 1.1.2.2.1 NAME 'producteInfo'
    SUP top AUXILIARY
    MAY ( productRating $ productFrom $ productBrand ))

~~~

Afegim les noves entitas al archiu edt-org.ldif:
~~~
# Crear la nueva OU
dn: ou=practica,dc=edt,dc=org
description: OU per a la practica
objectClass: organizationalUnit
ou: practica

# Crear una entrada de portátil bajo la nueva OU
dn: productName=IdealPad Lenovo,ou=practica,dc=edt,dc=org
objectClass: top
objectClass: producte
objectClass: producteInfo
productName: IdealPad Lenovo
productInform:< file:/opt/docker/files/documents/laptop.pdf
productPhoto:< file:/opt/docker/files/fotos/laptop.jpg
productStock: TRUE
productRating: 4
productFrom: Europa
productBrand: Lenovo

# Crear otra entrada de portátil bajo la nueva OU
dn: productName=Galaxy S21,ou=practica,dc=edt,dc=org
objectClass: top
objectClass: producte
objectClass: producteInfo
productName: Galaxy S21
productInform:< file:/opt/docker/files/documents/smartphone.pdf
productPhoto:< file:/opt/docker/files/fotos/smartphone.jpeg
productStock: FALSE
productRating: 3
productFrom: China
productBrand: Samsung

# Crear una entrada de móvil bajo la nueva OU
dn: productName=Apple Watch,ou=practica,dc=edt,dc=org
objectClass: top
objectClass: producte
objectClass: producteInfo
productName: Apple Watch
productInform:< file:/opt/docker/files/documents/applewatch.pdf
productPhoto:< file:/opt/docker/files/fotos/applewatch.jpeg
productStock: TRUE
productRating: 5
productFrom: Tailand
productBrand: Apple
~~~

# Construim la imatge

~~~
docker build -t diegomarcelo/ldap23:practica
~~~

# Utilitzacio

Fem servir un docker compose

~~~
version: "2"
services:
  ldap:
    image: diegomarcelo/ldap23:pract
    container_name: ldap
    hostname: ldapserver
    ports:
      - "389:389"
    networks:
      - mynet
  phpldapadmin:
    image: diegomarcelo/phpldapadmin
    container_name: php
    hostname: php.edt.org
    ports:
      - "80:80"
    networks:
      - mynet
networks:
  mynet:
~~~
