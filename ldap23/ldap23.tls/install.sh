#! /bin/bash
# @edt ASIX M06 2018-2019
# instal.lacio slapd edt.org
# -------------------------------------
cp  /opt/docker/ldap.conf /etc/ldap/ldap.conf
mkdir -p /etc/ldap/certs
cp /opt/docker/cacert.pem /etc/ldap/certs/.
cp /opt/docker/servercert.pem /etc/ldap/certs/.
cp /opt/docker/serverkey.pem  /etc/ldap/certs/.

rm -rf /etc/openldap/slapd.d/*
rm -rf /var/lib/ldap/*
cp /opt/docker/DB_CONFIG /var/lib/ldap
slaptest -F /etc/ldap/slapd.d -f /opt/docker/slapd.conf
echo "oscargay"
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d
chown -R openldap.openldap /var/lib/ldap
slapcat