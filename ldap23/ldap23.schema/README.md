# LDAP server
## @edt ASIX M06-ASO Curs 2022-2023

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


### Ldap servers:

 * **edtasixm06/ldap22:editat** 
   * generar un sol fitxer ldif anomenat edt.org.ldif
   * afegir en el fitxer dos usuaris i una ou nova inventada i posar-los dins aquesta ou.
   * modificar el fitxer edt.org.ldif  modificant dn dels usuaris utilitzant en lloc del cn el uid per identificar-los. 
   * configurar el password de Manager que sigui ‘secret’ però encriptat (posar-hi un comentari per indicar quin és de cara a estudiar).
   * propagar el port amb -p -P
   * editar els dos README, en el general afegir que tenim una nova imatge. En el de la imatge ldap21:editat descriure els canvis i les ordres per posar-lo en marxa.
   * [posteriorment] vam afegir el requeriment de que tingui cn=config]

#### Usuaris

Afegim els 11 usuaris i els dos usuairs demanats:

~~~
dn: uid=Brallan,ou=bailavini,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Braian Pou
sn: Pou
uid: brallan
uidNumber: 9000
gidNumber: 100
homeDirectory: /home/user
userPassword: brallanpou02

dn: uid=pauet,ou=bailavini,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
sn: Pou
uid: Pauet
uidNumber: 9001
gidNumber: 100
homeDirectory: /home/user
userPassword: paupou02

dn: cn=user0,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user0
sn: user0
uid: user0
uidNumber: 5000
gidNumber: 100
homeDirectory: /home/user
userPassword: passwd0
~~~

El user0 es el primer dels 12 que hi ha. Els usuaris pau i brallan es defineixen amb el uid al dn en comptes de amb el cn.

#### Generar passwd

Generem la passwd del manager amb l'ordre slappaswd
~~~
$ docker exec -it ldap.edt.org slappasswd
New password: 
Re-enter new password: 
{SSHA}oAtPEpCsAdk6SLqmqv+6fkqm2EHELq32
~~~

I afegim el text encryptat al slapd.conf com a passwd de la base edt.org.

~~~
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw {SSHA}Vn1sJ10sLiR5C8qxS9y1gEusDtn0efNU 
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
~~~

#### Engegar el container

Construim la imatge
~~~
docker build -t diegomarcelo/ldap23:edit .
~~~

Arranquem el container:
 ~~~
 docker run --rm --name cont -p 389:389 -it diegomarcelo/ldap23:edit
 ~~~

 ### BD config

Definim que poguem administrat la configuracio del serveri:

~~~
database config
rootdn "cn=Sysadmin,cn=config"
rootpw {SSHA}vwpQxtzc7yLsGg8K7fm02p2Fhox/PFP4
# el passwd es syskey
~~~